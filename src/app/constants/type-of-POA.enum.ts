export enum typeOfPOA {
    Durable = 'DURABLE',
    Temporary = 'TEMPORARY'
}