import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'poa-form-app';
  showStepper = false;

  createPOA() {
    this.showStepper = true;
  }
  
}
