import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, pipe, startWith, Subject, takeUntil } from 'rxjs';
import { AttorneySenderDTO } from '../services/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-create-poa',
  templateUrl: './create-poa.component.html',
  styleUrls: ['./create-poa.component.scss'],
  providers: [
    {
        provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError:true}
    }
]
})
export class CreatePoaComponent implements OnInit, OnDestroy {
  isLinear = true;
  userData$!: Observable<AttorneySenderDTO>;
  private destroy$ = new Subject();

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userData$ = this.userService.getUserDataById();
    // .pipe(takeUntil(this.destroy$))
  }

  ngOnDestroy(): void {
    // this.destroy$.next(true);
    // this.destroy$.complete();
  }

  submit(step1: any, step2: any) {

    console.log(step1, step2);

  }

}
