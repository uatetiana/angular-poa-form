import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePoaComponent } from './create-poa.component';

describe('CreatePoaComponent', () => {
  let component: CreatePoaComponent;
  let fixture: ComponentFixture<CreatePoaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePoaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreatePoaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
