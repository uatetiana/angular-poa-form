import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePoaStep2Component } from './create-poa-step2.component';

describe('CreatePoaStep2Component', () => {
  let component: CreatePoaStep2Component;
  let fixture: ComponentFixture<CreatePoaStep2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePoaStep2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreatePoaStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
