import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePoaStep3Component } from './create-poa-step3.component';

describe('CreatePoaStep3Component', () => {
  let component: CreatePoaStep3Component;
  let fixture: ComponentFixture<CreatePoaStep3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePoaStep3Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreatePoaStep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
