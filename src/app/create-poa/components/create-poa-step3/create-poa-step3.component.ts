import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-create-poa-step3',
  templateUrl: './create-poa-step3.component.html',
  styleUrls: ['./create-poa-step3.component.scss']
})
export class CreatePoaStep3Component implements OnInit {
  form: FormGroup = this.fb.group({
    nameOfPOA: [''],
    id: [''],
    cityOfPOA: [''],
    valueDate: ['']
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

}
