import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePoaStep4Component } from './create-poa-step4.component';

describe('CreatePoaStep4Component', () => {
  let component: CreatePoaStep4Component;
  let fixture: ComponentFixture<CreatePoaStep4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePoaStep4Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreatePoaStep4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
