import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePoaStep1Component } from './create-poa-step1.component';

describe('CreatePoaStep1Component', () => {
  let component: CreatePoaStep1Component;
  let fixture: ComponentFixture<CreatePoaStep1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePoaStep1Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreatePoaStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
