import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { typeOfPOA } from 'src/app/constants/type-of-POA.enum';
import { AttorneySenderDTO } from 'src/app/services/user.model';

@Component({
  selector: 'app-create-poa-step1',
  templateUrl: './create-poa-step1.component.html',
  styleUrls: ['./create-poa-step1.component.scss']
})
export class CreatePoaStep1Component implements OnInit, OnChanges {
  @Input() userData!: AttorneySenderDTO | null;
  form: FormGroup = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    phone: ['', Validators.required],
    email: ['', Validators.required],
    typeOfPOA: ['', Validators.required],
    range: this.fb.group({
      start: [null],
      end: [null],
    })
  });
  isRangeVisible = false;
  private destroy$ = new Subject();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form.get('typeOfPOA')?.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      if (value === typeOfPOA.Durable) {
        this.isRangeVisible = true;
      } else {
        this.isRangeVisible = false;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['userData'].currentValue) {
      this.form.patchValue(changes['userData'].currentValue);
    }
  }
}
