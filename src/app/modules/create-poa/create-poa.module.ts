import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreatePoaComponent } from 'src/app/create-poa/create-poa.component';
import { CreatePoaStep1Component } from 'src/app/create-poa/components/create-poa-step1/create-poa-step1.component';
import { CreatePoaStep2Component } from 'src/app/create-poa/components/create-poa-step2/create-poa-step2.component';
import { CreatePoaStep3Component } from 'src/app/create-poa/components/create-poa-step3/create-poa-step3.component';
import { CreatePoaStep4Component } from 'src/app/create-poa/components/create-poa-step4/create-poa-step4.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import { UserService } from 'src/app/services/user.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    CreatePoaComponent,
    CreatePoaStep1Component,
    CreatePoaStep2Component,
    CreatePoaStep3Component,
    CreatePoaStep4Component
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,
    SharedModule
    
  ],
  providers: [
    UserService
  ],
  exports: [
    CreatePoaComponent
  ]
})
export class CreatePoaModule { }
