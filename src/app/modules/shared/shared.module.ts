import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  declarations: [DialogComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    MatRadioModule
  ],
  exports: [DialogComponent]
})
export class SharedModule { }
