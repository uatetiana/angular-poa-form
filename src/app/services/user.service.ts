import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AttorneySenderDTO } from './user.model';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  getUserDataById(id?: string): Observable<AttorneySenderDTO> {
    return this.http.get<AttorneySenderDTO>('assets/json/user-data.json');
  }
}
