export interface AttorneySenderDTO { 
    id?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    phone?: string;
}
