# PoaFormApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

Acceptance criteria

 Acceptance criteria:
Worked stepper with dynamic steps / dynamic fields / Unit tests / Mocks / Toggles

 генеральная идея - степпер состоящий из 4 или 5 шагов

далее по шагам:
Приложение создания доверенности 

Step 1   Информация о создателе - поля - О нем мы знаем изначально так как якобы он залогинен (это мок) На этом шаге автозаполняются поле с First Name / Last Name / Telephone / Email / Type of power of attorney (тип доверенности)
Тип довренности - временная / безлимитная
Соответсвенно при выборе временной добавляются 2 поля для дат - с какого по какое число месяц год действительна доверенность

Step 2   Информация о получателе - Здесь поля - First Name / Last Name / Telephone / Country / City / Email
Дополнительно кнопка поиска пользователя
Т.е на этом шаге 2 варианта заполнения полей - ручная либо кнопка поиска пользователя
При использовании кнопки поиска пользователя открывается модалка с полями по которым можно произвести поиск - телефон или id получателя - при успешном поиске окно закрывается данные пользователя автозаполняются в основные поля, при негативном результате поиска - соответсвующее сообщение

Step 3 Данные доверенности - Имя доверенности / ID / Город действия доверенности / срок действия (автозаполняется с шага 1 если там было указано)

Step 4 Summary Таблица или лист общих данных что были введены о - Создателе / отправителе и детали доверенности


Дополнительный шаг - встраивается между 3 и 4 и содержит данные доверенного лица, что заверяет действительность доверенности First Name / Last Name / Telephone / Email / Type of power of attorney 
Соответственно при включенном тогле этот шаг будет номер 4 а Таблица или лист общих данных что были введены станет шагом 5


Дополнительно:
Дизайн не особо важен
Имя тоггла - confidant_toggle_1 
Сервисы - Сервис пользователя / Сервис тогла / Сервис поиска пользователя (Возможны дополнительные сервисы или наоборот уменьшение их количества. На усмотрение )
Под Каждый сервис и данные должны быть моки - пример мока для пользователя что создает доверенность:

export interface AttorneySenderDTO { 
    id?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
}

По такому же принципу построить интерфейсы для получателя / доверенности / доверенного лица
